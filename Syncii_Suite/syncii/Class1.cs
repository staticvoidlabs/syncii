﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace syncii
{
    class Class1
    {

        public static void SynchronizeDirectory(string sourceDirectory, string targetDirectory)
        {
            SynchronizeDirectory(new DirectoryInfo(sourceDirectory), new DirectoryInfo(targetDirectory));
        }

        private static void SynchronizeDirectory(DirectoryInfo source, DirectoryInfo target)
        {
            if (source.FullName == target.FullName || !source.Exists)
            {
                return;
            }
            if (!target.Exists)
            {
                Directory.CreateDirectory(target.FullName);
            }

            // Sync files. We want to keep track of files in source we’ve processed,
            // and keep a list of target files we have not seen in the process.
            List<string> targetFilesToDelete = target.GetFiles().Select(fi => fi.Name).ToList<string>(); //.GetHashCode();
            foreach (FileInfo sourceFile in source.GetFiles())
            {
                if (targetFilesToDelete.Contains(sourceFile.Name)) targetFilesToDelete.Remove(sourceFile.Name);

                var targetFile = new FileInfo(Path.Combine(target.FullName, sourceFile.Name));
                if (!targetFile.Exists)
                {
                    sourceFile.CopyTo(targetFile.FullName);
                }
                else if (sourceFile.LastWriteTime > targetFile.LastWriteTime)
                {
                    sourceFile.CopyTo(targetFile.FullName, true);
                }
            }
            foreach (var file in targetFilesToDelete)
            {
                File.Delete(Path.Combine(target.FullName, file));
            }

            // Sync folders.
            List<string> targetSubDirsToDelete = target.GetDirectories().Select(fi => fi.Name).ToList<string>();
            foreach (DirectoryInfo sourceSubDir in source.GetDirectories())
            {
                if (targetSubDirsToDelete.Contains(sourceSubDir.Name)) targetSubDirsToDelete.Remove(sourceSubDir.Name);
                DirectoryInfo targetSubDir = new DirectoryInfo(Path.Combine(target.FullName, sourceSubDir.Name));
                SynchronizeDirectory(sourceSubDir, targetSubDir);
            }
            foreach (var subdir in targetSubDirsToDelete)
            {
                Directory.Delete(Path.Combine(target.FullName, subdir), true);
            }
        }



    }
}
