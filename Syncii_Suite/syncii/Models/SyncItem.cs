﻿using System;
using System.Collections.Generic;
using System.Text;

namespace syncii.Models
{
    public class SyncItem
    {

        public string SourceFolder;
        public string DestFolder;
        public bool SyncRecursive;
        public bool DeleteFiles;

    }

}
