﻿using System;
using System.Collections.Generic;
using System.Text;

namespace syncii.Models
{
    public class SyncJobResult
    {

        public string Message;
        public bool HasJobFinishedSuccessfully;
        public int SyncErrorCounter;
        public int CountFilesSynced;
        public int CountFilesUpdated;
        public int CountFilesDeleted;

        public SyncJobResult()
        {
            Message = string.Empty;
            HasJobFinishedSuccessfully = false;
            SyncErrorCounter = 0;
            CountFilesSynced = 0;
            CountFilesUpdated = 0;
            CountFilesDeleted = 0;
        }


    }

}
