﻿using syncii.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace syncii
{
    public class SynciiLib
    {

        // Member definition.
        private string mSourceFolder;
        private string mDestFolder;
        private bool mRecursive;
        private bool mDeleteEnabled;
        private SyncJob mCurrentSyncJob;
        private int mCountJobsTotal;
        private int mCountJobsFinished;
        private int mCountFilesSynced;
        private int mCountFilesUpdated;
        private int mCountFilesDeleted;


        // Constructor
        public SynciiLib()
        {
        }


        // Delegate and event definition.
        public delegate void SyncJobEventHandler(object sender, SyncJobEventArgs e);
        public event SyncJobEventHandler SyncStateChanged;


        // Public methods.
        public SyncJobResult StartSyncJob(SyncJob syncJob)
        {

            SyncJobResult tmpSyncJobResult = new SyncJobResult();

            if (syncJob == null || syncJob.SyncItemList == null)
            {
                return tmpSyncJobResult;
            }

            mCurrentSyncJob = syncJob;
            mCountJobsTotal = mCurrentSyncJob.SyncItemList.Count;
            mCountJobsFinished = 0;
            mCountFilesSynced = 0;
            mCountFilesUpdated = 0;
            mCountFilesDeleted = 0;

            try 
            {

                if (mCurrentSyncJob.SyncItemList.Count == 0)
                {
                    SendSyncStateUpdate("Info: Empty Sync Job.");
                }

                foreach (var item in syncJob.SyncItemList)
                {
                    // Do syncing...
                    SendSyncStateUpdate("Info: Starting with job " + (mCountJobsFinished+1) + " of " + mCountJobsTotal + ".");

                    // ...
                    var sourceDir = new DirectoryInfo(item.SourceFolder);
                    var destDir = new DirectoryInfo(item.DestFolder);
                    SynchronizeDirectory(sourceDir, destDir);


                    SendSyncStateUpdate("Info: Finished job " + (mCountJobsFinished + 1) + " of " + mCountJobsTotal + ".");
                    mCountJobsFinished++;
                }

            }
            catch(Exception ex)
            {
                tmpSyncJobResult.Message = "Error: " + ex.Message;
            }

            tmpSyncJobResult.CountFilesSynced = mCountFilesSynced;
            tmpSyncJobResult.CountFilesUpdated = mCountFilesUpdated;
            tmpSyncJobResult.CountFilesDeleted = mCountFilesDeleted;

            return tmpSyncJobResult;
        }


        // Private methods.
        private void SynchronizeDirectory(DirectoryInfo source, DirectoryInfo target)
        {
            try
            {

                // Check if source and destination object are not null.
                if (source == null || target == null)
                {
                    return;
                }

                // Check if source and destination folders are the same.
                if (source.FullName == target.FullName || !source.Exists)
                {
                    return;
                }

                // Create destination folder if it not exists.
                if (!target.Exists)
                {
                    Directory.CreateDirectory(target.FullName);
                }

                // Start syncing files and folders recursively.
                // First check files in root folder.
                List<string> tmpTargetFilesToDelete = target.GetFiles().Select(fi => fi.Name).ToList<string>();
                foreach (FileInfo tmpSourceFile in source.GetFiles())
                {
                    if (tmpTargetFilesToDelete.Contains(tmpSourceFile.Name)) tmpTargetFilesToDelete.Remove(tmpSourceFile.Name);

                    var tmpTargetFile = new FileInfo(Path.Combine(target.FullName, tmpSourceFile.Name));

                    if (!tmpTargetFile.Exists)
                    {
                        tmpSourceFile.CopyTo(tmpTargetFile.FullName);
                        mCountFilesSynced++;
                    }
                    else if (tmpSourceFile.LastWriteTime > tmpTargetFile.LastWriteTime)
                    {
                        tmpSourceFile.CopyTo(tmpTargetFile.FullName, true);
                        mCountFilesUpdated++;
                    }
                }
                foreach (var tmpFile in tmpTargetFilesToDelete)
                {
                    File.Delete(Path.Combine(target.FullName, tmpFile));
                    mCountFilesDeleted++;
                }

                // Now check folders inside root folder.
                List<string> tmpTargetSubDirsToDelete = target.GetDirectories().Select(fi => fi.Name).ToList<string>();
                
                foreach (DirectoryInfo tmpSourceSubDir in source.GetDirectories())
                {
                    if (tmpTargetSubDirsToDelete.Contains(tmpSourceSubDir.Name)) tmpTargetSubDirsToDelete.Remove(tmpSourceSubDir.Name);
                    DirectoryInfo tmpTargetSubDir = new DirectoryInfo(Path.Combine(target.FullName, tmpSourceSubDir.Name));
                    SynchronizeDirectory(tmpSourceSubDir, tmpTargetSubDir);
                }
                
                foreach (var tmpSubdir in tmpTargetSubDirsToDelete)
                {
                    Directory.Delete(Path.Combine(target.FullName, tmpSubdir), true);
                }

            }
            catch(Exception ex)
            {
                throw;
            }
            
        }


        /// <summary>
        /// Event is raised in every update regarding sync job state.
        /// </summary>
        /// <param name="message"></param>
        private void SendSyncStateUpdate(string message)
        {
            try
            {
                // Build event args.
                SyncJobEventArgs e = new SyncJobEventArgs();
                e.message = message;

                // Raise event.
                SyncStateChanged?.Invoke(this, e);
            }
            catch (Exception ex)
            {
                throw;
            }
            
        }


    }
    
}
