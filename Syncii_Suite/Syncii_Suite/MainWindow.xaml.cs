﻿using ITunesLibraryParser;
using syncii;
using syncii.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Syncii_Suite
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            InitPlaylists();
        }

        private void InitPlaylists()
        {

            var library = new ITunesLibrary(@"C:\Users\alex\Music\iTunes\test.xml");

            var tracks = library.Tracks;
            var albums = library.Albums;
            var playlists = library.Playlists;

            foreach (var playlist in playlists)
            {
                lstAudioPlaylists.Items.Add(playlist.Name);
            }

        }

        private void StartSync(object sender, RoutedEventArgs e)
        {

            SynciiLib tmp1 = new SynciiLib();

            SyncJob tmpJob = new SyncJob();


            SyncItem tmpSyncItem = new SyncItem();
            tmpSyncItem.SourceFolder = @"C:\Daten_lokal\tmp\src";
            tmpSyncItem.DestFolder = @"C:\Daten_lokal\tmp\dst";
            tmpSyncItem.SyncRecursive = true;

            var tmpList = new List<SyncItem>();
            tmpList.Add(tmpSyncItem);

            tmpJob.SyncItemList = tmpList;

            tmp1.SyncStateChanged += OnSyncStateUpdateReceived;

            SyncJobResult tmpSyncJobResult = tmp1.StartSyncJob(tmpJob);





        }


        private void OnSyncStateUpdateReceived(object sender, SyncJobEventArgs e)
        {

            lstConsole.Items.Add(e.message);

        }

        private void OnClickSelectFolder(object sender, RoutedEventArgs e)
        {

        }
        
        /*
        private void OnClickSelectFolder(object sender, RoutedEventArgs e)
        {

            // Prepare dialog.
            var tmpDlg = new CommonOpenFileDialog();
            tmpDlg.Title = "Select folder";
            tmpDlg.IsFolderPicker = true;
            //tmpDlg.InitialDirectory = Environment.CurrentDirectory;
            //tmpDlg.AddToMostRecentlyUsedList = false;
            tmpDlg.AllowNonFileSystemItems = false;
            tmpDlg.DefaultDirectory = Environment.CurrentDirectory;
            tmpDlg.EnsureFileExists = true;
            tmpDlg.EnsurePathExists = true;
            tmpDlg.EnsureReadOnly = false;
            tmpDlg.EnsureValidNames = true;
            tmpDlg.Multiselect = false;
            tmpDlg.ShowPlacesList = true;

            // Process user selection.
            if (tmpDlg.ShowDialog() != CommonFileDialogResult.Ok || tmpDlg == null || string.IsNullOrWhiteSpace(tmpDlg.FileName))
            {
                return;
            }

            string tmpSelectedFolder = tmpDlg.FileName;

            // Check which textbox to fill.
            Button tmpButtonSender = (Button)sender;

            if (tmpButtonSender != null)
            {
                switch (tmpButtonSender.Name)
                {
                    case "btnSelectSourceVid": txtVidSource.Text = tmpSelectedFolder; break;
                    case "btnSelectDestVid": txtVidDest.Text = tmpSelectedFolder;  break;
                    case "btnSelectSourceAudio": txtAudioSource.Text = tmpSelectedFolder; break;
                    case "btnSelectDestAudio": txtAudioDest.Text = tmpSelectedFolder; break;
                    case "btnSelectSourcePic": txtPicSource.Text = tmpSelectedFolder; break;
                    case "btnSelectDestPic": txtPicDest.Text = tmpSelectedFolder; break;
                }

            }

        }
        */
        
        private void OnTogglePicAllAlbums(object sender, RoutedEventArgs e)
        {
            lstPicAlbums.IsEnabled = !lstPicAlbums.IsEnabled;
        }

    }
}
